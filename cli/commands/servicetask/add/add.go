package add

import (
	"context"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/shar-workflow/shar/cli/flag"
	"gitlab.com/shar-workflow/shar/cli/util"
	"gitlab.com/shar-workflow/shar/client/taskutil"
)

// Cmd is the cobra command object
var Cmd = &cobra.Command{
	Use:   "add",
	Short: "Adds a service task",
	Long:  ``,
	RunE:  run,
	Args:  cobra.ExactArgs(1),
}

func run(cmd *cobra.Command, args []string) error {
	if err := cmd.ValidateArgs(args); err != nil {
		return fmt.Errorf("invalid arguments: %w", err)
	}

	ctx := context.Background()
	shar := util.GetClient()
	if err := shar.Dial(ctx, flag.Value.Server); err != nil {
		return fmt.Errorf("dialling server: %w", err)
	}
	if _, err := taskutil.LoadTaskFromYamlFile(ctx, shar, args[0]); err != nil {
		return fmt.Errorf("load service task: %w", err)
	}
	if _, err := taskutil.RegisterTaskFunctionFromYamlFile(ctx, shar, args[0], nil); err != nil {
		return fmt.Errorf("register service task function: %w", err)
	}
	// output.Current.OutputSuccess()
	return nil
}

func init() {
	Cmd.Flags().StringVarP(&flag.Value.CorrelationKey, flag.CorrelationKey, flag.CorrelationKeyShort, "", "a correlation key for the message")
}
